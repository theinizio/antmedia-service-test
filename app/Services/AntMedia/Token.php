<?php


namespace App\Services\AntMedia;


class Token
{
    const TOKEN_TYPE_PUBLISH = 'publish';
    const TOKEN_TYPE_PLAY = 'play';

    /** @var string $tokenId */
    public $tokenId;
    /** @var string $streamId */
    public $streamId;

    public $expireDate;
    public $type;

    public function __construct($tokenId = null, $streamId = null, $expireDate = null, $type = null)
    {
        $this->tokenId = $tokenId;
        $this->streamId = $streamId;
        $this->expireDate = $expireDate;
        $this->type = $type;
    }

    public static function createFromResponse(object $response): Token
    {
        return new self($response->tokenId, $response->streamId, $response->expireDate, $response->type);
    }


}