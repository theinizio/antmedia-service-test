<?php

namespace App\Services;

use App\Models\UserStream;
use App\Services\AntMedia\Stream;
use App\Services\AntMedia\Token;
use GuzzleHttp\Client;


class AntMediaService
{
    /** Default Ant application name for live streaming "live" */
    const DEFAULT_APPLICATION_NAME = '...';
    const BASE_URI = '...';
    const ORIGIN_ADDRESS = '...';

    const DEFAULT_PAGINATION_SIZE = 20;
    const STREAM_STATE_FINISHED = 'finished';
    const STREAM_STATE_BROADCASTING = 'broadcasting';
    const STREAM_STATE_CREATED = 'created';


    /** @var array|\stdClass $apiAnswer - answer from API */
    private $apiAnswer = [];

    private $client;

    /** @var array $errors - array of errors of component */
    private $errors = [];

    /**
     * AntComponent constructor.
     */
    public function __construct()
    {
        $this->client = new Client(['base_uri' => self::BASE_URI,]);
    }


    /**
     * @return string
     */
    public function getDefaultApplicationName(): string
    {
        return self::DEFAULT_APPLICATION_NAME;
    }

    /**
     * Create new stream (for new Publisher means)
     * @param UserStream $streamParams
     * @return Stream
     */
    public function createStream(UserStream $streamParams = null): Stream
    {
        $data = [
            'name' => $streamParams->stream_name??'',
            'username' => $streamParams->getWowzaLogin()??'',
            'password' => $streamParams->getWowzaPass()??'',
        ];

        return Stream::create($data);
    }

    /**
     * @param string $streamName
     * @return Stream
     */
    public function getStream($streamId):Stream
    {
        return new Stream($streamId);
    }

    public function updateStream($streamId, $data)
    {
        $request = $this->client->put($streamId, ['json' => $data]);

        if ($request->getStatusCode() === 200 && json_decode($request->getBody())->success === true) {
            return $this->getStream($streamId);
        }

        return (string)$request->getBody();
    }

    public function deleteStream(string $streamId)
    {
        $request = $this->client->request('DELETE', $streamId);

        if ($request->getStatusCode() === 200) {
            return json_decode($request->getBody());
        }

        return (string)$request->getBody();
    }


    public function stopStream($streamId)
    {
        $request = $this->client->post($streamId . '/stop');

        if ($request->getStatusCode() === 200 && json_decode($request->getBody())->success === true) {
            return $this->getStream($streamId);
        }

        return (string)$request->getBody();
    }

    public function streamState(string $streamId):string
    {
        return $this->getStream($streamId)->status;
    }

    public function createToken(string $streamId, string $type=Token::TOKEN_TYPE_PLAY, int $expireDate=null):Token
    {
        return (new Stream($streamId))->createToken($type, $expireDate);
    }

    public function addConferenceToRoom(string $streamId):bool
    {
        if ($streamId) {
            return (new Stream($streamId))->addConferenceRoom();
        }
        return false;
    }

    public function deleteConferenceFromRoom(string $streamId):bool
    {
        return (new Stream($streamId))->deleteConferenceRoom();
    }

}
